# Git for TFS users
Lab 10: Rewriting history with interactive rebase 

---

# Tasks

 - Create some local commits
 
 - Rewriting history with interactive rebase

---

## Preparations

 - Clone a repository to work with:
 
```
http://<server-ip>:8080/tfs/DefaultCollection/git-workshop/_git/demo-app-lab-10
```

---

## Create some local commits

  - Create some local commits using echo:
```
$ echo commit 1 >> file1.txt
$ git add file1.txt
$ git commit -m "commit 1"
```
```
$ echo commit 2 >> file2.txt
$ git add file2.txt
$ git commit -m "commit 2"
```
```
$ echo commit 3 >> file3.txt
$ git add file3.txt
$ git commit -m "commit 3"
```
```
$ echo commit 4 >> file4.txt
$ git add file4.txt
$ git commit -m "commit 4"
```
```
$ echo commit 5 >> file5.txt
$ git add file5.txt
$ git commit -m "commit 5"
```

 - Let's see the changes using:
```
$ git log --oneline --decorate
```

---

## Rewriting history with interactive rebase

 - Run the command below to start the interactive rebase:
```
$ git rebase -i
```

 - Configure the rebase to do the below (in vim use "i" to edit and "ESC" + ":wq" to save the changes):
```
1) Change the commits order, "commit 4" should be the first one
2) Squash "commit 2" with "commit 1"
3) Delete "commit 3"
4) Edit the commit message of "commit 5"
```
```
pick    <SHA1>  commit 4
pick    <SHA1>  commit 1
squash  <SHA1>  commit 2
drop    <SHA1>  commit 3
reword  <SHA1>  commit 5
```

 - You will be asked to set the commit message for the squashed commit, set it as below:
```
# This is a combination of 2 commits

squashed commit

# Please enter the commit message for your changes.
```

 - You will be asked to set the commit message for the commit 5, set it as below:
```
new commit message

# Please enter the commit message for your changes.
```

 - Let's see the changes using:
```
$ git log --oneline --decorate
```

